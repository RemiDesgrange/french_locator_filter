# Manual

This is a so called QgsLocator filter for the French Adress API search service, implemented as a QGIS plugin.

This plugin is forked from the [Nominatim Locator Filter](https://plugins.qgis.org/plugins/nominatim_locator_filter/) plugin, by Richard Duivenvoorde (Zuidt). Read more about the  it: <https://qgis.nl/2018/05/16/english-coding-a-qgslocator-plugin/?lang=en>.
