<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>FrenchBanGeocoderLocatorFilter</name>
    <message>
        <location filename="../../core/locator_filter.py" line="85"/>
        <source>French Adress geocoder</source>
        <translation>Géocodeur API BAN</translation>
    </message>
    <message>
        <location filename="../../core/locator_filter.py" line="199"/>
        <source>Result triggerred by the user received: {}</source>
        <translation>Résultat sélectionné par l&apos;utilisateur reçu : {}</translation>
    </message>
    <message>
        <location filename="../../core/locator_filter.py" line="208"/>
        <source>Something went wrong during result deserialization: {}. Trying another method...</source>
        <translation>Impossible de récupérer le résultat sélection : {}. Essai par une autre méthode...</translation>
    </message>
    <message>
        <location filename="../../core/locator_filter.py" line="142"/>
        <source>API search not triggered. Reason: </source>
        <translation>Recherche API non déclenchée. Raison : </translation>
    </message>
    <message>
        <location filename="../../core/locator_filter.py" line="120"/>
        <source>minimum chars {} not reached: {}</source>
        <translation>nombre minimum de caractére {} non atteint : {}</translation>
    </message>
    <message>
        <location filename="../../core/locator_filter.py" line="133"/>
        <source>search term is matching the prefix.</source>
        <translation>le terme de recherche correspond au préfixe.</translation>
    </message>
    <message>
        <location filename="../../core/locator_filter.py" line="142"/>
        <source>Search term &apos;{}&apos; is one of special terms to be ignored.</source>
        <translation>le terme &apos;{}&apos; est l&apos;un des termes spéciaux à ignorer.</translation>
    </message>
</context>
<context>
    <name>dlg_settings</name>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="26"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="88"/>
        <source>Features</source>
        <translation>Fonctionnalités</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="194"/>
        <source>HTTP user-agent:</source>
        <translation>HTTP user-agent :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="156"/>
        <source>Request URL:</source>
        <translation>Base de l&apos;URL de requête :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="175"/>
        <source>Request parameters:</source>
        <translation>Paramètres de la requête :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="356"/>
        <source>Miscellaneous</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="372"/>
        <source>Enable debug mode.</source>
        <translation>Activer le mode debug.</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="381"/>
        <source>Debug mode (degraded performances)</source>
        <translation>Activer le mode debug (performances dégradées)</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="365"/>
        <source>Version used to save settings:</source>
        <translation>Paramètres sauvegardés avec la version :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="403"/>
        <source>Report an issue</source>
        <translation>Signaler une anomalie</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="439"/>
        <source>Help</source>
        <translation>Aide en ligne</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="125"/>
        <source>Minimal search length</source>
        <translation>Nombre minimal de caractères avant de déclencher la requête à l&apos;API</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="131"/>
        <source> characters</source>
        <translation> caractères</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="293"/>
        <source>Minimal search length:</source>
        <translation>Longueur minimale de la recherche :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="245"/>
        <source>HTTP content type:</source>
        <translation>Type de contenu HTTP :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="436"/>
        <source>Open documentation</source>
        <translation>Ouvrir l&apos;aide en ligne du plugin sur votre navigateur</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="315"/>
        <source>List of strings to ignore in search bar, separated by commas.</source>
        <translation>Liste de termes à ignorer dans la berre de recherche, séparés par des virgules.</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="458"/>
        <source>Reset to factory default</source>
        <translation>Réinitialise les paramètres par défaut</translation>
    </message>
</context>
</TS>
