#! python3  # noqa: E265

"""
    Locator Filter.
"""

# standard library
import json

# PyQGIS
from qgis.core import (
    QgsCoordinateReferenceSystem,
    QgsCoordinateTransform,
    QgsFeedback,
    QgsLocatorContext,
    QgsLocatorFilter,
    QgsLocatorResult,
    QgsPointXY,
    QgsProject,
)
from qgis.gui import QgisInterface
from qgis.PyQt.QtWidgets import QWidget
from qgis.utils import iface

# project
from french_locator_filter.__about__ import __title__
from french_locator_filter.toolbelt import (
    NetworkRequestsManager,
    PlgLogger,
    PlgOptionsManager,
)

# ############################################################################
# ########## Classes ###############
# ##################################


class FrenchBanGeocoderLocatorFilter(QgsLocatorFilter):
    """QGIS Locator Filter subclass.

    :param iface: An interface instance that will be passed to this class which \
    provides the hook by which you can manipulate the QGIS application at run time.
    :type iface: QgisInterface
    """

    def __init__(self, iface: QgisInterface = iface):
        self.iface = iface
        self.log = PlgLogger().log
        self.plg_settings = PlgOptionsManager.get_plg_settings()

        super(QgsLocatorFilter, self).__init__()

    def name(self) -> str:
        """Returns the unique name for the filter. This should be an untranslated \
        string identifying the filter.

        :return: filter unique name
        :rtype: str
        """
        return self.__class__.__name__

    def hasConfigWidget(self) -> bool:
        """Should return True if the filter has a configuration widget.

        :return: configuration widget available
        :rtype: bool
        """
        return True

    def clone(self) -> QgsLocatorFilter:
        """Creates a clone of the filter. New requests are always executed in a clone \
        of the original filter.

        :return: clone of the actual filter
        :rtype: QgsLocatorFilter
        """
        return FrenchBanGeocoderLocatorFilter(iface)

    def displayName(self) -> str:
        """Returns a translated, user-friendly name for the filter.

        :return: user-friendly name to be displayed
        :rtype: str
        """
        return self.tr("French Adress geocoder")

    def prefix(self) -> str:
        """Returns the search prefix character(s) for this filter. Prefix a search with \
        these characters will restrict the locator search to only include results from \
        this filter.

        :return: search prefix for the filter
        :rtype: str
        """
        return "fra"

    def fetchResults(
        self, search: str, context: QgsLocatorContext, feedback: QgsFeedback
    ):
        """Retrieves the filter results for a specified search string. The context \
        argument encapsulates the context relating to the search (such as a map extent \
        to prioritize). \

        Implementations of fetchResults() should emit the resultFetched() signal \
        whenever they encounter a matching result. \
        Subclasses should periodically check the feedback object to determine whether \
        the query has been canceled. If so, the subclass should return from this method \
        as soon as possible. This will be called from a background thread unless \
        flags() returns the QgsLocatorFilter.FlagFast flag.

        :param search: text entered by the end-user into the locator line edit
        :type search: str
        :param context: [description]
        :type context: QgsLocatorContext
        :param feedback: [description]
        :type feedback: QgsFeedback
        """
        # ignore if search terms is inferior than minimum number of chars
        if len(search) < self.plg_settings.min_search_length:
            self.log(
                message=self.tr("API search not triggered. Reason: ")
                + self.tr(
                    "minimum chars {} not reached: {}".format(
                        self.plg_settings.min_search_length, len(search)
                    )
                ),
                log_level=4,
            )
            return

        # ignore if search terms is equal to the prefix
        if search.rstrip() == self.prefix:
            self.log(
                message=self.tr("API search not triggered. Reason: ")
                + self.tr("search term is matching the prefix."),
                log_level=4,
            )
            return

        # ignore if search terms is one of special terms to ignore
        if search.strip() in self.plg_settings.search_terms_to_ignore:
            self.log(
                message=self.tr("API search not triggered. Reason: ")
                + self.tr(
                    "Search term '{}' is one of special terms to be ignored.".format(
                        search
                    )
                ),
                log_level=4,
            )
            return

        # request
        try:
            qntwk = NetworkRequestsManager()
            qurl = qntwk.build_url(additional_query=f"&q={search}")
            response_content = qntwk.get_url(url=qurl)
        except Exception as err:
            self.log(message=err, log_level=1)
            return

        # process response
        try:
            # load response as a dict
            locations = json.loads(str(response_content, "UTF8"))

            # loop on features in json collection
            for loc in locations.get("features"):
                result = QgsLocatorResult()
                result.filter = self
                label = loc.get("properties").get("label")
                if loc.get("properties").get("type") == "municipality":
                    # add city code to label
                    label += " " + loc.get("properties").get("citycode")
                result.displayString = label
                result.group = loc.get("properties").get("type")

                # use the json full item as userData, so all info is in it:
                result.userData = loc
                self.resultFetched.emit(result)
        except Exception:
            self.log(message="Response processing failed.", log_level=1)
            return

    def triggerResult(self, result: QgsLocatorResult):
        """Triggers a filter result from this filter. This is called when one of the \
        results obtained by a call to fetchResults() is triggered by a user. \
        The filter subclass must implement logic here to perform the desired operation \
        for the search result. E.g. a file search filter would open file associated \
        with the triggered result.

        :param result: result selected by user
        :type result: QgsLocatorResult
        """
        # Newer Version of PyQT does not expose the .userData (Leading to core dump)
        # Try via get Function, otherwise access attribute
        try:
            doc = result.getUserData()
            self.log(
                message=self.tr(
                    "Result triggerred by the user received: {}".format(
                        doc.get("properties").get("label")
                    )
                ),
                log_level=4,
            )
        except Exception as err:
            self.log(
                message=self.tr(
                    "Something went wrong during result deserialization: {}. "
                    "Trying another method...".format(err)
                ),
                log_level=2,
            )
            doc = result.userData

        x = doc["geometry"]["coordinates"][0]
        y = doc["geometry"]["coordinates"][1]

        centerPoint = QgsPointXY(x, y)
        dest_crs = QgsProject.instance().crs()
        results_crs = QgsCoordinateReferenceSystem.fromEpsgId(4326)
        coords_transform = QgsCoordinateTransform(
            results_crs, dest_crs, QgsProject.instance()
        )
        centerPointProjected = coords_transform.transform(centerPoint)
        coords_transform.transform(centerPoint)

        # centers to adress coordinates
        iface.mapCanvas().setCenter(centerPointProjected)

        # zoom policy has we don't have extent in the results
        scale = 25000

        type_adress = doc.get("properties").get("type")

        if type_adress == "housenumber":
            scale = 2000
        elif type_adress == "street":
            scale = 5000
        elif type_adress == "locality":
            scale = 5000

        # finally zoom actually
        iface.mapCanvas().zoomScale(scale)
        iface.mapCanvas().refresh()

    def openConfigWidget(self, parent: QWidget = None):
        """Opens the configuration widget for the filter (if it has one), with the \
        specified parent widget. self.hasConfigWidget() must return True.

        :param parent: prent widget, defaults to None
        :type parent: QWidget, optional
        """
        iface.showOptionsDialog(parent=parent, currentPage=f"mOptionsPage{__title__}")
