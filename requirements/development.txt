# Develop matching the development guidelines
# -------------------------------------------

black
flake8-builtins>=1.5,<1.6
flake8-eradicate>=1.0,<1.3
flake8-isort>=4.0,<4.2
flake8-qgis>=1,<1.1
pre-commit>=2.17,<2.20
